﻿using System;
using System.Collections.Generic;

namespace HomeworkFour
{
    class Program
    {
        public static String[] playerClass = { "Barbarian", "Bard", "Cleric", "Druid", "Fighter", "Monk", "Paladin", "Ranger", "Rogue", "Sorcerer", "Warlock", "Wizard", "Random" };
        public static String[] racial = { "Dragonborne", "Dwarf", "Elf", "Gnome", "Half-Elf", "Halfing", "Half-Orc", "Human", "Tiefling", "Random" };
        public static String[] background = { "Acolyte", "Charlatan", "Criminal", "Entertainer", "Folk Hero", "Guild Artisan", "Hermit", "Outlander", "Noble", "Sage", "Sailor", "Soldier", "Urchin", "Random" };

        public static void Main()
        {
            //Console.Write(askPref("class", playerClass) + " " + askPref("racial", racial) + " " + askPref("background", background));

            Character player = new Character(askPref("class", playerClass), askPref("racial", racial), askPref("background", background));

            PrintCharacter(player);
        }

        public static String askPref(String typeName, String[] type)
        {
            Random rand = new Random();

            Console.Clear();
            Console.WriteLine("Please indicated " + typeName + " preference");
            for (int i = 0; i < type.Length; i++) { Console.WriteLine(i + ". " + type[i]); }
            Console.WriteLine("\n Type corresponding number: ");
            try
            {

                int selection = Convert.ToInt16(Console.ReadLine());
                if (selection == type.Length - 1) { return type[rand.Next(type.Length - 1)]; }
                return type[selection];
            }

            catch (Exception) { return (type[rand.Next(type.Length - 1)]); }

        }

        public static void PrintCharacter(Character player){

            Console.Clear();
            player.DisplayerAll();


        }












        public class Character
        {

            public Character(String pclass, String race, String background){

                PlayerClass = pclass;
                Race = race;
                Background = background;
                stats = new Dictionary<string, int>();
                setStats();
                adjustStats(Race);

                prof = new Profiencies(this ,race, background, pclass);


            }

            public void DisplayerAll(){
                Console.Clear();
                DisplayStats();
                Console.Write("\n");
                prof.display();

            }

            private void setStats(){

                stats.Add("STR", roll()); stats.Add("DEX", roll());
                stats.Add("CON", roll()); stats.Add("INT", roll());
                stats.Add("WIS", roll()); stats.Add("CHR", roll());

            } //sets the character's base stats based on random rolls

            private int roll()
            {
                Random rand = new Random();

                int lowestValue = 6;
                int sum = 0;

                for (int i = 0; i < 4; i++)
                {
                    int current = rand.Next(6) + 1;
                    sum += current;
                    if (current < lowestValue) { lowestValue = current; }
                }
                return (sum - lowestValue);

            } // rolls the random rolls for the stats

            private void adjustStats(String racial)
            {
                switch (racial)
                {

                    case "Dragonborne":
                        stats["STR"] += 2; stats["CHR"] += 1;
                        break;
                    case "Dwarf":
                        stats["CON"] += 2;
                        break;
                    case "Elf":
                        stats["DEX"] += 2;
                        break;
                    case "Gnome":
                        stats["INT"] += 2;
                        break;
                    case "Half-Elf":
                        stats["CHR"] += 2;
                        Console.Clear();
                        Console.WriteLine("You are a Half-Elf, this means that you get +1 to two ability scores other than Charisma");
                        DisplayStats();

                        Console.WriteLine("\n Please indicated two values you'd like to increase.");
                        String FirstAttribute = "";
                        String SecondAttribute = "";
                        while (FirstAttribute == "")
                        {
                            try
                            {
                                Console.Write("\nFirst: ");
                                FirstAttribute = Console.ReadLine();
                                if (!stats.ContainsKey(FirstAttribute)) { FirstAttribute = ""; }
                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Please give a proper answer.");
                            }
                        }
                        while (SecondAttribute == "")
                        {
                            try
                            {
                                Console.Write("\nSecond: ");
                                SecondAttribute = Console.ReadLine();
                                if (!stats.ContainsKey(SecondAttribute)) { SecondAttribute = ""; }
                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Please give a proper answer.");
                            }
                        }
                        stats[FirstAttribute] += 1;
                        stats[SecondAttribute] += 1;
                        break;
                    case "Halfing":
                        stats["DEX"] += 2;
                        break;
                    case "Half-Orc":
                        stats["STR"] += 2; stats["CON"] += 1;
                        break;
                    case "Human":
                        stats["STR"] += 1; stats["DEX"] += 1;
                        stats["CON"] += 1; stats["INT"] += 1;
                        stats["WIS"] += 1; stats["CHR"] += 1;
                        break;
                    case "tiefling":
                        stats["CHR"] += 2; stats["INT"] += 1;
                        break;

                }
            } //applies racial bonuses to stats

            public void DisplayStats()
            {
                Console.WriteLine("\nSTR: " + stats["STR"]);
                Console.WriteLine("DEX: " + stats["DEX"]);
                Console.WriteLine("CON: " + stats["CON"]);
                Console.WriteLine("INT: " + stats["INT"]);
                Console.WriteLine("WIS: " + stats["WIS"]);
                Console.WriteLine("CHA: " + stats["CHR"]);

            } // displays the stats of the character in the terminal 

            public String PlayerClass { get; set; }
            public String Race { get; set; }
            public String Background { get; set; }
            public Dictionary<String, int> stats { get; }
            public String[][] profiencies { get; set; }
            public Profiencies prof { get; }
        }









        public class Profiencies
        {
            public Dictionary<String, Boolean> weapon { get; set; }
            public Dictionary<String, Boolean> armor { get; set; }
            public Dictionary<String, Dictionary<String, Boolean>> stat { get; set; }
            public Dictionary<String, Boolean> saving { get; set; }
            public Character player { get; }

            public Profiencies(Character player_, String race, String Background, String playerClass){
                weapon = new Dictionary<string, bool>();
                armor = new Dictionary<string, bool>();
                stat = new Dictionary<string, Dictionary<string, bool>>();
                saving = new Dictionary<string, bool>();
                player = player_;


                weapon.Add("Martial", false);
                weapon.Add("Light", false);



                armor.Add("Heavy", false);
                armor.Add("Medium", false);
                armor.Add("Light", false);
                armor.Add("Shields", false);


                stat.Add("STR", new Dictionary<String, Boolean>());
                saving.Add("STR", false);
                stat["STR"].Add("Athletics", false);

                stat.Add("DEX", new Dictionary<String,Boolean>());
                saving.Add("DEX", false);
                stat["DEX"].Add("Acrobatics", false);
                stat["DEX"].Add("Slight of Hand", false);
                stat["DEX"].Add("Stealth", false);

                saving.Add("CON", false);

                stat.Add("INT", new Dictionary<String, Boolean>());
                saving.Add("INT", false);
                stat["INT"].Add("Arcana", false);
                stat["INT"].Add("History", false);
                stat["INT"].Add("Investigation", false);
                stat["INT"].Add("Nature", false);
                stat["INT"].Add("Religion", false);

                stat.Add("WIS", new Dictionary<String, Boolean>());
                saving.Add("WIS", false);
                stat["WIS"].Add("Animal Handeling", false);
                stat["WIS"].Add("Insight", false);
                stat["WIS"].Add("Medicine", false);
                stat["WIS"].Add("Perception", false);
                stat["WIS"].Add("Survival", false);

                stat.Add("CHR", new Dictionary<String, Boolean>());
                saving.Add("CHR", false);
                stat["CHR"].Add("Deception", false);
                stat["CHR"].Add("Intimidation", false);
                stat["CHR"].Add("Performance", false);
                stat["CHR"].Add("Persuasion", false);

                AdjustProf(playerClass);
            }

            public void AdjustProf(string playerClass){

                switch (playerClass){
                    case "Barbarian":
                        armor["Light"] = true;
                        armor["Medium"] = true;
                        armor["Shields"] = true;
                        weapon["Martial"] = true;
                        weapon["Simple"] = true;
                        saving["STR"] = true;
                        saving["CON"] = true;

                        //choose two from Animal Handeling, Athletics, Intimidation, Nature, Perception, Survival
                        break;

                    case "Bard":
                        armor["Light"] = true;
                        weapon["simple"] = true;
                        saving["DEX"] = true;
                        saving["CHR"] = true;

                        //choose any three skills
                        break;

                    case "Cleric":
                        armor["Light"] = true;
                        armor["Medium"] = true;
                        armor["Shields"] = true;
                        weapon["simple"] = true;
                        saving["WIS"] = true;
                        saving["CHR"] = true;

                        //choose two from History, Insight, Medicine, Persuasion, Religion

                        break;

                    case "Druid":
                        armor["Light"] = true;
                        armor["Medium"] = true;
                        armor["Shields"] = true;
                        saving["INT"] = true;
                        saving["WIS"] = true;

                        //choose two Arcana, Animal Handeling, Insight, Medicine, Nature, Perception, Religion, Survival
                        break;

                    case "Fighter":
                        armor["Light"] = true;
                        armor["Medium"] = true;
                        armor["Shields"] = true;
                        weapon["Simple"] = true;
                        weapon["Martial"] = true;
                        saving["STR"] = true;
                        saving["CON"] = true;

                        //Two from Acrobatics, Animal Handeling, Atheltics, History, Insight, Intimidation, Perception, Survival
                        break;

                    case "Monk":
                        weapon["Simple"] = true;
                        saving["STR"] = true;
                        saving["DEX"] = true;

                        //Two from Acrobatics, Athletics, History, Insight, Religion, Stealth
                        break;

                    case "Paladin":
                        armor["Light"] = true;
                        armor["Medium"] = true;
                        armor["Shields"] = true;
                        weapon["Simple"] = true;
                        weapon["Martial"] = true;
                        saving["WIS"] = true;
                        saving["CHR"] = true;
                        
                        //Two from Athletics, Insight, Intimidation, Medicine, Religion, Persuasion
                        break;

                    case "Ranger":
                        armor["Light"] = true;
                        armor["Medium"] = true;
                        armor["Shields"] = true;
                        weapon["Simple"] = true;
                        weapon["Martial"] = true;
                        saving["STR"] = true;
                        saving["DEX"] = true;

                        //three from Animal Handeling, Athletics, Insight, Investigation, Nature, Perception, Stealth, and Survival
                        break;

                    case "Rogue":
                        armor["Light"] = true;
                        weapon["Simple"] = true;
                        saving["DEX"] = true;
                        saving["INT"] = true;

                        //four from Acrobatics, Athletics, Deception, Insight, Intimidation, Investigation, Perception, Performance, Persuasion, Slight of Hand, Stealth 
                        break;

                    case "Sorcerer":
                        saving["CON"] = true;
                        saving["CHR"] = true;

                        //two from Arcana, Deception, Insight, Intimidation, Persuasion, Religion
                        break;

                    case "Warlock":
                        armor["Light"] = true;
                        weapon["Simple"] = true;
                        saving["WIS"] = true;
                        saving["CHR"] = true;

                        //two from Arcana, Deception, History, Intimidation Investiation, Nature. Religion
                        break;

                    case "Wizard":
                        saving["INT"] = true;
                        saving["WIS"] = true;

                        //choose two from Arcana, History, Insight, Investigation, Medicine, Religion
                        break;
                }
            }

            public void ChooseSkills(String[] skills, int numOfSkills){
            } //not yet implamented

            public void display(){
                String[] playerStats = { "STR", "DEX", "CON", "INT", "WIS", "CHR" };
                foreach (String stat_ in weapon.Keys){ Console.WriteLine(stat_ + ": " + weapon[stat_]); }
                foreach (String stat_ in armor.Keys) { Console.WriteLine(stat_ + ": " + armor[stat_]); }
                foreach (String stat_ in playerStats)
                {

                    Console.WriteLine("\n\n" + stat_ + " saving: +" + getBonus(stat_,saving[stat_]));
                    try
                    {
                        foreach (String skill in stat[stat_].Keys)
                        {
                            Console.WriteLine(skill + ": +" + getBonus(stat_,stat[stat_][skill]));
                        }
                    }catch (Exception) { }

                }

            }

            public String getBonus(String baseSkill,Boolean addProf){
                int rvalue = 0;
                rvalue += getMod(player.stats[baseSkill]);
                if(addProf){ rvalue += 2; }

                return rvalue.ToString();


            }

            private int getMod(int score){
                switch (score) {
                    case 1:
                        return -5;
                    case 2:

                    case 3:
                        return -4;
                    case 4:

                    case 5:
                        return -3;
                    case 6:
                        
                    case 7:
                        return -2;
                    case 8:

                    case 9:
                        return -1;
                    case 10:

                    case 11:
                        return 0;
                    case 12:

                    case 13:
                        return 1;
                    case 14:

                    case 15:
                        return 2;
                    case 16:

                    case 17:
                        return 3;
                    case 18:

                    case 19:
                        return 4;
                    case 20:
                        return 5;
                    default:
                        return 0;
                }
            }
        }
    }
}

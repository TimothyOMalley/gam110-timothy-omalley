﻿using System;
using System.Collections.Generic;

namespace Sorting
{
    class MainClass
    {
        public static int itterations;
        public static void Main(string[] args)
        {
            itterations = 0;

            List<int> nums = Generate(10);
            Display(nums);
            Console.Write("\n\n\n");
            BubbleSort(nums);

            Display(nums);
            Console.Write("\n itterations: " + itterations);
            

        }

        public static List<int> Generate(int nums){
            Random rand = new Random();
            List<int> ret = new List<int>();
            for(int i = 0; i < nums; i++)
            {
                ret.Add(rand.Next(1000));
            }
            return ret;
        }

        public static void Display(List<int> vars)
        {
            for(int i = 0; i < vars.Count; i++)
            {
                Console.Write("\n" + vars[i]);

            }
        }

        public static List<int> BubbleSort(List<int> nums){
            int waterline = nums.Count - 1;
            Boolean sorted = false;
            while (sorted == false)
            {
                sorted = true;
                for (int i = 0; i < waterline; i++)
                {
                    itterations++;
                    int numOne = nums[i];
                    int numTwo = nums[i + 1];
                    if (numOne > numTwo)
                    {
                        sorted = false;
                        nums[i] = numTwo;
                        nums[i + 1] = numOne;
                    }
                }
            }
            return nums;

        }
    }
}

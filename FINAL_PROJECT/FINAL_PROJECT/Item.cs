﻿using System;
namespace FINAL_PROJECT
{
    public class Item
    {
        public int durability;
        public int damage;

        public Item(int strength, int dmg){
            durability = strength;
            damage = dmg;
        }

        public void printInfo()
        {
            Console.WriteLine("     durability: " + durability);
            Console.WriteLine("     damage:     " + damage);
        }
    }
}

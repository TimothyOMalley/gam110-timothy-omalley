﻿using System;
using System.Collections.Generic;
using System.IO;

namespace FINAL_PROJECT
{
    class MainClass
    {
        public static Dictionary<String, Item> catalogue = new Dictionary<string, Item>(); //collection of items
        public static Boolean brk = true;
        public static void Main()
        {
            Load();
            while (brk)
            {
                Action();
            }
            
        }

        public static void Load()
        {
            StreamReader input = new StreamReader("Saved Items.txt");
            while(input.Peek() != -1) 
            {
                try
                {
                    catalogue.Add(input.ReadLine(),new Item(Int32.Parse(input.ReadLine()), Int32.Parse(input.ReadLine())));
                }
                catch (Exception) { }
            }
            input.Close();
        }

        public static void Save()
        {
            StreamWriter output = new StreamWriter("Saved Items.txt");
            foreach (String item in catalogue.Keys) {
                output.WriteLine(item);
                output.WriteLine(catalogue[item].durability);
                output.WriteLine(catalogue[item].damage);
                    }
            output.Close();
        }

        public static void Action() //player choice 
        {
            Console.Clear();
            Console.WriteLine("1. Make a tool");
            Console.WriteLine("2. Show all tools");
            Console.WriteLine("ESC. Save and Quit");
            Console.WriteLine("9. Erase data and Quit");

            switch (Console.ReadKey().Key)
            {
                case ConsoleKey.D1:

                    Console.Clear();

                    string name = "";
                    Console.WriteLine("What will this item be named?");
                    try { name = Console.ReadLine(); } catch (Exception) { Action(); }
                    //tries to get a name this time, same output if fail.

                    int str = 0;
                    Console.WriteLine("How durable will this item be? (a number)");
                    try { str = Int32.Parse(Console.ReadLine()); } catch (Exception) { Action(); }
                    //then durability

                    int dmg = 0;
                    Console.WriteLine("How much damage will this do? (a number)");
                    try { dmg = Int32.Parse(Console.ReadLine()); } catch (Exception) { Action(); }
                    //then damage

                    Item temp = new Item(str,dmg);
                    if (catalogue.ContainsKey(name)) { Console.WriteLine("Sorry, " + name + " already exists."); }
                    else { catalogue.Add(name, temp); }
                    break;

                case ConsoleKey.D2:
                    Console.Clear();
                    foreach (String title in catalogue.Keys)
                    {
                        Console.WriteLine(title);
                        catalogue[title].printInfo();

                    }
                    Console.ReadKey();
                    break;

                case ConsoleKey.Escape:
                    Save();
                    brk = false;
                    break;

                case ConsoleKey.D9:
                    brk = false;
                    StreamWriter output = new StreamWriter("Saved Items.txt");
                    output.Flush();
                    break;


                default:
                    Action();
                    break;
            }
        }
    }
}

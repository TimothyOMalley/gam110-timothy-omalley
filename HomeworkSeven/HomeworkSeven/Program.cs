﻿using System;
using System.Collections.Generic;
using System.IO;

namespace HomeworkSeven
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            StreamReader reader = new StreamReader("WarAndPeace.txt");
            Dictionary<String, int> words = new Dictionary<String, int>();
            while (reader.Peek() != -1){

                String line = reader.ReadLine();
                foreach (String word in line.Split(' ')){
                    if (words.ContainsKey(word)) { words[word] += 1; }
                    else { words.Add(word, 1); }
                }
            }
            reader.Close();

            StreamWriter writer = new StreamWriter("Output.txt");
            foreach(KeyValuePair<String,int> word in words){
                Console.WriteLine(word.Key + ": " + word.Value);
                writer.WriteLine(word.Key + ": " + word.Value);
            }
            writer.Close();

        }
    }
}

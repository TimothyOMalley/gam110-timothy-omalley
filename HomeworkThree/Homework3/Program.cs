﻿using System;



 public class HomeworkThree{
    public static void Main(){
        String[] information = new String[3];
        information = ObtainInformation(information);
        UnitConverstion(information);
        Console.ReadKey();
    }

    public static String[] ObtainInformation(String[] information){
        while(information[0] == null){
            Console.WriteLine("Which unit would you like to convert from?");
            Console.WriteLine("\n1. Centimetes");
            Console.WriteLine("\n2. Inches");
            Console.WriteLine("\n3. Feet");
            Console.WriteLine("\n4. Meters");
            Console.WriteLine("\n5. Miles");

            switch (Console.ReadKey(false).Key){

                case ConsoleKey.D1:
                    information[0] = "Centimeters";
                    break;
                case ConsoleKey.D2:
                    information[0] = "Inches";
                    break;
                case ConsoleKey.D3:
                    information[0] = "Feet";
                    break;
                case ConsoleKey.D4:
                    information[0] = "Meters";
                    break;
                case ConsoleKey.D5:
                    information[0] = "Miles";
                    break;
                default:
                    Console.Clear();
                    break;
            }
        }

        Console.Clear();

        while (information[1] == null){

            Console.WriteLine("Which unit would you like to convert to?");
            Console.WriteLine("\n1. Centimetes");
            Console.WriteLine("\n2. Inches");
            Console.WriteLine("\n3. Feet");
            Console.WriteLine("\n4. Meters");
            Console.WriteLine("\n5. Miles");

            switch (Console.ReadKey(false).Key)
            {

                case ConsoleKey.D1:
                    information[1] = "Centimeters";
                    break;
                case ConsoleKey.D2:
                    information[1] = "Inches";
                    break;
                case ConsoleKey.D3:
                    information[1] = "Feet";
                    break;
                case ConsoleKey.D4:
                    information[1] = "Meters";
                    break;
                case ConsoleKey.D5:
                    information[1] = "Miles";
                    break;
                default:
                    Console.Clear();
                    break;
            }
        }

        Console.Clear();

        while(information[2] == null){
            Console.WriteLine("How many " + information[0] + " would you like to convert to "+information[1]+"?");
            try{
                double testVariable = Convert.ToDouble(Console.ReadLine());
                information[2] = testVariable.ToString();
            } catch(Exception){
                Console.Clear();
                Console.WriteLine("Please give a valid numerical value.\n");
            }
        }

        return information;
    }


    public static void UnitConverstion(String[] information){
        Console.Clear();
        Console.Write(information[2]+" "+information[0]+" = ");
        switch(information[0]){ // from unit
            case "Centimeters":
                switch(information[1]){ //to unit
                    case "Centimeters": //Centimeters to Centimeters
                        Console.Write(information[2]+" "+information[1]);
                        break;
                    case "Inches": //Centimeters to Inches
                        Console.Write(Convert.ToString(Convert.ToDouble(information[2]) / 2.52) + " " + information[1]);
                        break;
                    case "Feet": //Centimeters to Feet
                        Console.Write(Convert.ToString(Convert.ToDouble(information[2]) / 30.48) + " " + information[1]);
                        break;
                    case "Meters": //Centimeters to Meters
                        Console.Write(Convert.ToString(Convert.ToDouble(information[2]) / 100) + " " + information[1]);
                        break;
                    case "Miles": //Centimeters to Miles
                        Console.Write(Convert.ToString(Convert.ToDouble(information[2]) / 160934.4) + " " + information[1]);
                        break;
                }
                break;
            case "Inches":
                switch (information[1]) //to unit
                {
                    case "Centimeters": //Inches to Centimeters
                        Console.Write(Convert.ToString(Convert.ToDouble(information[2]) * 2.52) + " " + information[1]);
                        break;
                    case "Inches": //Inches to Inches
                        Console.Write(information[2]+ " " + information[1]);
                        break;
                    case "Feet": //Inches to Feet
                        Console.Write(Convert.ToString(Convert.ToDouble(information[2]) / 12) + " " + information[1]);
                        break;
                    case "Meters": //Inches to Meters
                        Console.Write(Convert.ToString(Convert.ToDouble(information[2]) / 39.37) + " " + information[1]);
                        break;
                    case "Miles": //Inches to Miles
                        Console.Write(Convert.ToString(Convert.ToDouble(information[2]) / 63360) + " " + information[1]);
                        break;
                }
                break;
            case "Feet":
                switch (information[1]) // to unit
                {
                    case "Centimeters": //Feet to Centimeters
                        Console.Write(Convert.ToString(Convert.ToDouble(information[2]) * 30.48) + " " + information[1]);
                        break;
                    case "Inches": //Feet to Inches
                        Console.Write(Convert.ToString(Convert.ToDouble(information[2]) * 12) + " " + information[1]);
                        break;
                    case "Feet": //Feet to Feet
                        Console.Write(information[2] + " " + information[1]);
                        break;
                    case "Meters": //Feet to Meters
                        Console.Write(Convert.ToString(Convert.ToDouble(information[2]) / 3.281) + " " + information[1]);
                        break;
                    case "Miles": //Feet to Miles
                        Console.Write(Convert.ToString(Convert.ToDouble(information[2]) / 5280) + " " + information[1]);
                        break;
                }
                break;
            case "Meters":
                switch (information[1]) //to unit
                {
                    case "Centimeters": //Meters to Centimeters
                        Console.Write(Convert.ToString(Convert.ToDouble(information[2]) * 100) + " " + information[1]);
                        break;
                    case "Inches": //Meters to Inches
                        Console.Write(Convert.ToString(Convert.ToDouble(information[2]) * 39.37) + " " + information[1]);
                        break;
                    case "Feet": //Meters to Feet
                        Console.Write(Convert.ToString(Convert.ToDouble(information[2]) * 3.281) + " " + information[1]);
                        break;
                    case "Meters": //Meters to Meters
                        Console.Write(information[2] + " " + information[1]);
                        break;
                    case "Miles": //Meters to Miles
                        Console.Write(Convert.ToString(Convert.ToDouble(information[2]) / 1609.344) + " " + information[1]);
                        break;
                }
                break;
            case "Miles":
                switch (information[1]) //to unit
                {
                    case "Centimeters": //Miles to Centimeters
                        Console.Write(Convert.ToString(Convert.ToDouble(information[2]) * 160934.4) + " " + information[1]);
                        break;
                    case "Inches": //Miles to Inches
                        Console.Write(Convert.ToString(Convert.ToDouble(information[2]) * 63360) + " " + information[1]);
                        break;
                    case "Feet": //Miles to Feet
                        Console.Write(Convert.ToString(Convert.ToDouble(information[2]) * 5280) + " " + information[1]);
                        break;
                    case "Meters": //Miles to Meters
                        Console.Write(Convert.ToString(Convert.ToDouble(information[2]) * 1609.344) + " " + information[1]);
                        break;
                    case "Miles": //Miles to Miles
                        Console.Write(information[2] + " " + information[1]);
                        break;
                }
                break;
        }
        Console.Write("\n\n");
        Console.ReadKey();
        Main();
    }
 }

﻿using System;

public class HomeworkTwo
{
    public static void Main()
    {

        string startingUnit;
        string endingUnit;

        Console.Write("Please indicate starting unit of mesurment."); //obtain first unit of measurment from user
        startingUnit = ObtainUnit();

        Console.Write("Please indicate ending unit of measurment."); //obtain second unit of measurment from user
        endingUnit = ObtainUnit();

        //Get user input and output conversion result
        Console.WriteLine(endingUnit + ": " + UnitConversion(startingUnit, endingUnit));

        Console.ReadKey();
    }

    public static String ObtainUnit()
    {
        Console.Write("\n(inches, feet, yards)\n"); // prompt user  of possible inputs
        switch (Console.ReadLine())
        {
            case "inches":
                Console.Clear();
                return "inches";
            case "feet":
                Console.Clear();
                return "feet";
            case "yards":
                Console.Clear();
                return "yards";
            default:
                Console.Clear(); //if invalid input, clear and re-run ObtainUnit()
                return ObtainUnit();
        }
    }

    public static double UnitConversion(String starting, String end)
    {
        double startingVar = 0;
        while (true)
        {
            Console.Write(starting + ": "); //i.e "Yards: " ; prompting for user to input inital vlue to be converted

            try //try-catch to ensure whatever is entered is a number
            {
                startingVar = Convert.ToDouble(Console.ReadLine());
                break;
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine("please give a valid number");
            }
        }


        if (starting.Equals("inches"))
        {
            if (end.Equals("inches")) { return startingVar; } //inches to inches
            else if (end.Equals("feet")) { return (startingVar / 12); } // inches to feet
            else { return (startingVar / 36); } //inches to yards
        }
        else if (starting.Equals("feet"))
        {
            if (end.Equals("inches")) { return (startingVar * 12); } //feet to inches
            else if (end.Equals("feet")) { return startingVar; } //feet to feet
            else { return (startingVar / 3); } //feet to yards
        }
        else
        {
            if (end.Equals("inches")) { return (startingVar * 36); } //yards to inches
            else if (end.Equals("feet")) { return (startingVar * 3); } //yards to feet
            else { return startingVar; } //yards to yards
        }
    }
}

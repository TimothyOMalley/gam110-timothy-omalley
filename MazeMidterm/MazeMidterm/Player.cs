﻿using System;
namespace MazeMidterm{

    public class Player{

        public int keys{ get; set; }
        public int[] playerPos { get; set; }

        public Player(Maze platform){
            keys = 0;
            playerPos = platform.findPlayer();
        }

        public void Play(Maze platform){
            Boolean plock = true;
            while (plock == true){
                switch (Console.ReadKey().Key)
                {
                    case ConsoleKey.W:
                        
                    case ConsoleKey.UpArrow:
                        Move(platform, "Left");
                        break;
                    case ConsoleKey.A:

                    case ConsoleKey.LeftArrow:
                        Move(platform, "Up");
                        break;
                    case ConsoleKey.S:
                    
                    case ConsoleKey.DownArrow:
                        Move(platform, "Right");
                        break;
                    case ConsoleKey.D:
                        
                    case ConsoleKey.RightArrow:
                        Move(platform, "Down");
                        break;
                    case ConsoleKey.Escape:
                        break;
                    default:
                        break;
                }
                platform.display();
            }
        }

        void Move(Maze platform,String dir) {
            switch(dir){
                case "Up":
                    if (playerPos[1] == 0) { }
                    else {
                        if (platform.platform[playerPos[0], playerPos[1] - 1] == 'x')
                        {}
                        else
                        {
                            platform.platform[playerPos[0], playerPos[1]] = ' ';
                            playerPos[1] -= 1;
                            platform.platform[playerPos[0], playerPos[1]] = 'p';
                        }
                    }
                    break;
                case "Right":
                    if (playerPos[0] == platform.dimensions[0]-1) { }
                    else
                    {
                        if (platform.platform[playerPos[0]+1, playerPos[1]] == 'x')
                        { }
                        else
                        {
                            platform.platform[playerPos[0], playerPos[1]] = ' ';
                            playerPos[0] += 1;
                            platform.platform[playerPos[0], playerPos[1]] = 'p';
                        }
                    }
                    break;
                case "Left":
                    if (playerPos[0] == 0) { }
                    else
                    {
                        if (platform.platform[playerPos[0] - 1, playerPos[1]] == 'x')
                        { }
                        else
                        {
                            platform.platform[playerPos[0], playerPos[1]] = ' ';
                            playerPos[0] -= 1;
                            platform.platform[playerPos[0], playerPos[1]] = 'p';
                        }
                    }
                    break;
                case "Down":
                    if (playerPos[1] == platform.dimensions[1]-1) { }
                    else
                    {
                        if (platform.platform[playerPos[0], playerPos[1] + 1] == 'x')
                        { }
                        else{
                            platform.platform[playerPos[0], playerPos[1]] = ' ';
                            playerPos[1] += 1;
                            platform.platform[playerPos[0], playerPos[1]] = 'p';
                        }
                    }
                    break;
            }
        }

    
    }
}

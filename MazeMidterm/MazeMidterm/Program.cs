﻿using System;

namespace MazeMidterm
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Maze platform = new Maze();
            platform.display();
            Player user = new Player(platform);
            user.Play(platform);
        }
    }
}

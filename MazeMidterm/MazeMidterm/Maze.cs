﻿using System;
using System.Collections.Generic;
namespace MazeMidterm{

    public class Maze{

        public String difficulty { get; set; }
        public int[] dimensions { get; set; }
        public char[,] platform { get; }

        public Maze(){
            dimensions = new int[2];
            dimensions[0] = 10;
            dimensions[1] = 10;
            platform = GenerateMaze(platform);
        }


        public char[,] GenerateMaze(char[,] plat ){
            char[,] maze = {
                {'x','p','x','x','x','x','x','x','x','x'},
                {'x',' ','x',' ',' ',' ','x',' ',' ','x'},
                {'x',' ',' ',' ','x',' ','x',' ','x','x'},
                {'x','x',' ','x',' ',' ','x',' ','x','x'},
                {'x','x',' ','x','x','x',' ',' ',' ','x'},
                {'x',' ',' ','x',' ','x',' ','x',' ','x'},
                {'x',' ','x',' ',' ',' ',' ','x',' ','x'},
                {'x',' ','x','x',' ','x',' ','x',' ',' '},
                {'x',' ',' ',' ',' ','x',' ',' ','x','x'},
                {'x','x','x','x','x','x','x','x','x','x'}
            };

            return maze;

        }


        public void display(){
            Console.Clear();
            Console.WriteLine("\n\n");

            for (int x = 0; x < dimensions[1]; x++){

                for (int y = 0; y < dimensions[0]; y++){
                    if(platform[x,y] == 'x'){
                        Console.BackgroundColor = ConsoleColor.DarkBlue;
                        Console.Write("  ");
                    }
                    else{
                        if (platform[x, y] == 'k')
                        {
                            Console.BackgroundColor = ConsoleColor.Yellow;
                            Console.Write("  ");
                        }
                        else
                        {
                            if (platform[x, y] == 'l')
                            {
                                Console.BackgroundColor = ConsoleColor.Gray;
                                Console.Write("  ");
                            }
                            else {
                                if (platform[x, y] == 'p')
                                {
                                    Console.BackgroundColor = ConsoleColor.Green;
                                    Console.Write("  ");
                                }
                                else
                                {
                                    Console.ResetColor();
                                    Console.Write("  ");
                                }
                            }
                        }
                    }
                }
                Console.ResetColor();
                Console.WriteLine("");
            }
        }

        public int[] findPlayer() {
            int[] cords = { 0, 0 };

            for(int i = 0; i < dimensions[0]-1; i++)
            {
                for(int j = 0;j<dimensions[1]-1; j++)
                {
                    if(platform[i,j] == 'p')
                    {
                        cords[0] = i;
                        cords[1] = j;
                        return cords;
                    }
                }
            }
            return cords;
        }
    }
}

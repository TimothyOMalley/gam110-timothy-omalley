﻿using System;

namespace TickTackToe
{
    class MainClass
    {
        public static Boolean brk = true;
        public static char turnOrder = 'x';
        public static void Main(string[] args)
        {
            Board player = new Board();
            while (brk)
            {
                player.Refresh();
                player.TakeTurn(turnOrder);
                if(turnOrder == 'x') { turnOrder = 'o'; }
                else { turnOrder = 'x'; }
            }
        }
    }
}

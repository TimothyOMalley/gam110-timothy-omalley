﻿using System;
namespace TickTackToe
{
    public class Board
    {
        public char[,] brd;
        public Board()
        {
            brd = new char[3, 3];
            brd[0, 0] = '1'; brd[0, 1] = '2'; brd[0, 2] = '3';
            brd[1, 0] = '4'; brd[1, 1] = '5'; brd[1, 2] = '6';
            brd[2, 0] = '7'; brd[2, 1] = '8'; brd[2, 2] = '9';
        }

        public void Refresh()
        {
            Console.Clear();
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine("              ");
            Console.Write("  ");
            Console.ResetColor();
            Console.Write(brd[0, 0]);
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write("   ");
            Console.ResetColor();
            Console.Write(brd[0, 1]);
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write("   ");
            Console.ResetColor();
            Console.Write(brd[0, 2]);
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write("   \n");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine("              ");
            Console.Write("  ");
            Console.ResetColor();
            Console.Write(brd[1, 0]);
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write("   ");
            Console.ResetColor();
            Console.Write(brd[1, 1]);
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write("   ");
            Console.ResetColor();
            Console.Write(brd[1, 2]);
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write("   \n");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine("              ");
            Console.Write("  ");
            Console.ResetColor();
            Console.Write(brd[2, 0]);
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write("   ");
            Console.ResetColor();
            Console.Write(brd[2, 1]);
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write("   ");
            Console.ResetColor();
            Console.Write(brd[2, 2]);
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write("   \n");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine("              ");
            Console.ResetColor();

            Console.Write("\n\nPick a square: ");

        }

        public void TakeTurn(char character)
        {
            
            switch (Console.ReadKey().Key)
            {
                case ConsoleKey.D1:
                    if(brd[0,0] == '1') { brd[0, 0] = character; }
                    else { TakeTurn(character); }
                    break;
                case ConsoleKey.D2:
                    if (brd[0, 1] == '2') { brd[0, 1] = character; }
                    else { TakeTurn(character); }
                    break;
                case ConsoleKey.D3:
                    if (brd[0, 2] == '3') { brd[0, 2] = character; }
                    else { TakeTurn(character); }
                    break;
                case ConsoleKey.D4:
                    if (brd[1, 0] == '4') { brd[1, 0] = character; }
                    else { TakeTurn(character); }
                    break;
                case ConsoleKey.D5:
                    if (brd[1, 1] == '5') { brd[1, 1] = character; }
                    else { TakeTurn(character); }
                    break;
                case ConsoleKey.D6:
                    if (brd[1, 2] == '6') { brd[1, 2] = character; }
                    else { TakeTurn(character); }
                    break;
                case ConsoleKey.D7:
                    if (brd[2, 0] == '7') { brd[2, 0] = character; }
                    else { TakeTurn(character); }
                    break;
                case ConsoleKey.D8:
                    if (brd[2,1] == '8') { brd[2, 1] = character; }
                    else { TakeTurn(character); }
                    break;
                case ConsoleKey.D9:
                    if (brd[2, 2] == '9') { brd[2, 2] = character; }
                    else { TakeTurn(character); }
                    break;
                default:
                    TakeTurn(character);
                    break;
            }
        }
    }
}
